console.log("hello from js");

let loginForm = document.querySelector('#loginUser')


loginForm.addEventListener("submit", (e) => {
	e.preventDefault() 

	let email = document.querySelector("#userEmail").value
	console.log(email)
	let password = document.querySelector("#password").value
	console.log(password)

	//how can we inform the user that a blank input field cannot be logged in? - gawa tayo control structure

	if(email == "" || password == "") {
		alert("Please input your email and/or password.")
	}else{
		fetch('http://localhost:4000/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(res => {
			return res.json()
		}).then(data => {
			console.log(data.access)
			if(data.access){
				//lets save the access token inside our local storage.
				localStorage.setItem('token', data.access)
				//this local storage can be found inside the subfolder of the appData of the local file of the google chrome browser inside the data module of the user folder.
				// \AppData\Local\Google\Chrome\User Data\Default\Local Storage
				alert("Successfully Login.") //2-19
				//educational purpose only.
				fetch(`http://localhost:4000/api/users/details`, {
					headers: {
						'Authorization': `Bearer ${data.access}`
					}
				}).then(res => {
					return res.json()
				}).then(data => {
					//lets create a checker to see if may nakukuhang data.
					console.log(data)
					localStorage.setItem("id", data._id) //this came from the payload.
					localStorage.setItem("isAdmin", data.isAdmin)
					console.log("item are set inside the local storage.")
					//direct the user to the courses page upon successful login.
					window.location.replace('./courses.html') //2/18
				})
			}else{
				//if there is no existing access key value from the data variable then just inform the user
				alert("Something went wrong, Check your credentials")
			}
		})
	}

})