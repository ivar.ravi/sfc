//console.log("link to the js module")
//Advance task answer key:

let formSubmit = document.querySelector("#createCourse")


formSubmit.addEventListener("submit", (event) => { //line 59
	event.preventDefault() //which is use to avoid automatic page redirection.
	//lets capture all of the information inside the form.
	//.value describe the value property of the input field
	//.value => describes the value attribute of the HTML element.
	let name = document.querySelector("#courseName").value
	//console.log(name)
	let cost = document.querySelector("#coursePrice").value
	//console.log(cost)
	let desc = document.querySelector("#courseDescription").value
	//console.log(desc)

	// save the new entry inside the database. by describing the request method.

// 	fetch('http://localhost:4000/api/courses/addCourse', {
// 		method: 'POST',
// 		headers: {
// 			'Content-Type': 'application/json'
// 		},
// 		body: JSON.stringify({
// 			name: name,
// 			description: desc,
// 			price: cost
// 		})
// 	})
// 	.then(res => {return res.json() //after describing the structure of the request body, now create the structure of the response coming from the backend. //so that it can be readable at the browser line 32
// 	})
// 	.then(info => {
// 		if(info === true){
// 			alert('A New is Successfully Created')
// 		}else {
// 			alert('Something Went Wrong when adding a new course')
// 		}
// 	})
// })

//task 2/15

if(name !=='' && cost!==null && desc!==''){
    fetch('http://localhost:4000/api/courses/course-exists', {
       method: 'POST',
       headers: {
           'Content-Type': 'application/json'
       },
       body: JSON.stringify({
            name: name
       })
    }).then(res => res.json()
        ) //this will give the information if there are no duplicates found.
       .then(data => {
          if(data === false){
             fetch('http://localhost:4000/api/courses/addCourse', {
            method: 'POST',
            headers: {
               'Content-Type': 'application/json'
           },	
           body: JSON.stringify({
               name: name,
               price: cost,
               description: desc
           }) //this section describe the body of the request converted into a JSON format. 
        }).then(res => {
          return res.json()
        }).then(data => {
        }).then(data => {
          console.log(data)
          if(data === true){
            alert("Something went wrong in the registration") //New Course Added in Database Successfully
      }else {	
         alert("New Course Added in Database Successfully") //Something went wrong in the registration
          }
        })
          }else{
             alert("Course already exist")
          }
       })
	}else{
	alert("Please check your input.")
	}
})